package middleware

import (
	"gallery-api/models"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
)

func RequiredUser(ug models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		header = strings.TrimSpace(header)
		if len(header) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}

		token := strings.TrimSpace(header[len("Bearer "):])
		if token == "" {
			log.Printf(("NO token ===================="))
			c.JSON(401, gin.H{
				"message": "invalid token",
			})
			c.Abort()
			return
		}

		user, err := ug.GetByToken(token)
		if err != nil {
			log.Printf(("no user ===================="))
			c.JSON(401, gin.H{
				"message": "unauthorized",
			})
			c.Abort()
			return
		}
		c.Set("user", user)
	}
}
