package models

import "github.com/jinzhu/gorm"

//Gallery is table
type Gallery struct {
	gorm.Model
	Name string
	UserID uint
	Status bool
}

type GalleryService interface {
	CreateGallery(gallery *Gallery) error
	ListGallery() ([]Gallery, error)
	DeleteGallery(id uint) error
	UpdateGallery(gallery *Gallery) error
	GetGalleryById(id uint) (*Gallery, error)
	GetByID(id uint) (*Gallery, error)
	UpdateGalleryPublishing(id uint, isPublish bool) error
	//have to be the same **
}

var _ GalleryService = &GalleryGorm{}

type GalleryGorm struct {
	db *gorm.DB
}

func NewGalleryGorm(db *gorm.DB) GalleryService {
	return &GalleryGorm{db}
}

//same with this **
func (gg *GalleryGorm) CreateGallery(gallery *Gallery) error {
	return gg.db.Create(gallery).Error
}

func (gg *GalleryGorm) ListGallery() ([]Gallery, error) {
	galleries := []Gallery{}
	if err := gg.db.Find(&galleries).Error; err != nil {
		return nil, err
	}
	return galleries , nil
}

func (gg *GalleryGorm) GetGalleryById(id uint) (*Gallery, error) {
	gt := new(Gallery)
	if err := gg.db.First(gt, id).Error; err != nil {
		return nil, err
	}
	return gt, nil
}

func (gg *GalleryGorm) DeleteGallery(id uint) error {
	gt := new(Gallery)
	if err := gg.db.Where("id = ?", id).First(gt).Error; err != nil {
		return err
	}
	return gg.db.Delete(gt).Error
}

func (gg *GalleryGorm) UpdateGallery(gallery *Gallery) error {
	found := new(Gallery)
	if err := gg.db.Where("id = ?", gallery.ID).First(found).Error; err != nil {
		return err
	}
	return gg.db.Model(gallery).Update("Name", gallery.Name).Error
}

func (gg *GalleryGorm) GetByID(id uint) (*Gallery, error) {
	gallery := new(Gallery)
	if err := gg.db.Where("id = ?", id).First(gallery).Error; err != nil {
		return nil, err
	}
	return gallery, nil
}

func (gg *GalleryGorm) UpdateGalleryPublishing(id uint, Status bool) error {
	return gg.db.Model(&Gallery{}).Where("id = ?", id).Update("status", Status).Error
}