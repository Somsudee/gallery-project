package handlers

import (
	"gallery-api/models"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

//Gallery is table
type Gallery struct {
	gorm.Model
	ID     uint   `json: "id"`
	Name   string `json: "name"`
	UserID uint   `json: "user_id"`
	Status bool   `json: "status"`
}

type CreateGallery struct {
	Name   string `json: "name"`
	UserID uint   `json: "user_id"`
	Status bool   `json: "status"`
}

type UpdateGallery struct {
	Name   string `json: "name"`
	UserID uint   `json: "user_id"`
	Status bool   `json: "status"`
}

type GetGallery struct {
	ID     uint   `json: "id"`
	Name   string `jsom: "name"`
	UserID uint   `json: "user_id"`
	Status bool   `json: "status"`
}

type GalleryHandler struct {
	gs models.GalleryService
}

func NewGalleryHandler(gs models.GalleryService) *GalleryHandler {
	return &GalleryHandler{gs}
}

//Create gallery
func (gh *GalleryHandler) CreateGallery(c *gin.Context) {
	data := new(CreateGallery)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallery := new(models.Gallery)
	gallery.Name = data.Name
	gallery.UserID = data.UserID
	if err := gh.gs.CreateGallery(gallery); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"id":      gallery.ID,
		"name":    gallery.Name,
		"user_id": gallery.UserID,
		"status":  gallery.Status,
	})
}

//Get gallery
func (gh *GalleryHandler) ListGallery(c *gin.Context) {
	gg, err := gh.gs.ListGallery()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	galleries := []Gallery{}
	for _, g := range gg {
		galleries = append(galleries, Gallery{
			ID:     g.ID,
			Name:   g.Name,
			UserID: g.UserID,
			Status: g.Status,
		})
	}
	c.JSON(200, galleries)
}

// Delete gallery
func (gh *GalleryHandler) DeleteGallery(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	if err := gh.gs.DeleteGallery(uint(id)); err != nil {
		log.Printf((err.Error()))
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

// Update Gallery
func (gh *GalleryHandler) UpdateGallery(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"mesage": err.Error(),
		})
	}

	gallery := new(UpdateGallery)
	if err := c.BindJSON(gallery); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	found, err := gh.gs.GetGalleryById(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	found.Name = gallery.Name
	if err := gh.gs.UpdateGallery(found); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.Status(204)
}

func (gh *GalleryHandler) GetOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	oneGallery, err := gh.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, oneGallery)
}

type UpdateStatusReq struct {
	Status bool `json:"status"`
}

func (gh *GalleryHandler) UpdatePublishing(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err = gh.gs.UpdateGalleryPublishing(uint(id), req.Status)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}
