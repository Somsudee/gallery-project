package handlers

import (
	"gallery-api/header"
	"gallery-api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	ug models.UserService
}

func NewUserHandler(ug models.UserService) *UserHandler {
	return &UserHandler{ug}
}

//Create user
func (uh *UserHandler) CreateUser(c *gin.Context) {
	type UserReq struct {
		Token     string `json: "token"`
		Email     string `json: "email"`
		Password  string `json: "password"`
		FirstName string `json: "firstname"`
		LastName  string `json: "lastname"`
		Gender    string `json: "gender"`
	}
	var req = new(UserReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.FirstName = req.FirstName
	user.LastName = req.LastName
	user.Gender = req.Gender
	user.Password = req.Password
	if err := uh.ug.CreateUser(user); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": user.Token,
	})
}

type LoginReq struct {
	Email    string `json: "email"`
	Password string `json: "password"`
}

func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	token, err := uh.ug.Login(user)
	if err != nil {
		c.JSON(401, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (uh *UserHandler) GetSession(c *gin.Context) {
	user, ok := c.Value("user").(*models.User)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	token := header.GetToken(c)
	if token == "" {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	user, err := uh.ug.GetByToken(token)
	if err != nil {
		c.JSON(401, gin.H{
			"message": "user not found",
		})
		return
	}
	c.JSON(201, user)
}

func (uh *UserHandler) Logout(c *gin.Context) {
	token := header.GetToken(c)
	if token == "" {
		c.JSON(401, gin.H{
			"message": "user not found",
		})
		return
	}
	err := uh.ug.Logout(token)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}
