package handlers

import (
	"gallery-api/models"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Image struct {
	ID        uint   `json: "id"`
	Filename  string `json: "filename"`
	GalleryID uint   `json: "gallery_id"`
}

type CreateImage struct {
	Filename  string `json: "filename"`
	GalleryID uint   `json: "gallery_id"`
}

type CreateImageRes struct {
	Image
}

type ImageHandler struct {
	gs  models.GalleryService
	ims models.ImageService
}

func NewImageHandler(gs models.GalleryService, ims models.ImageService) *ImageHandler {
	return &ImageHandler{gs, ims}
}

//create Image
func (ih *ImageHandler) CreateImage(c *gin.Context) {
	galleryIDStr := c.Param("id")
	galleryID, err := strconv.Atoi(galleryIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	gallery, err := ih.gs.GetByID(uint(galleryID))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	images, err := ih.ims.CreateImages(form.File["photos"], gallery.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := []CreateImageRes{}
	for _, img := range images {
		r := CreateImageRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = filepath.Join(models.UploadPath, galleryIDStr, img.Filename)
		res = append(res, r)
	}

	c.JSON(201, res)
}

//delete
func (ih *ImageHandler) DeleteImage(c *gin.Context) {
	imageIDStr := c.Param("id")
	id, err := strconv.Atoi(imageIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := ih.ims.Delete(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(http.StatusOK)
}

type ListGalleryImagesRes struct {
	Image
}

func (ih *ImageHandler) ListGalleryImages(c *gin.Context) {
	galleryIDStr := c.Param("id")
	id, err := strconv.Atoi(galleryIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	gallery, err := ih.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	images, err := ih.ims.GetByGalleryID(gallery.ID)
	if err != nil {
		c.JSON( http.StatusNotFound, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []ListGalleryImagesRes{}
	for _, img := range images {
		r := ListGalleryImagesRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = filepath.Join(models.UploadPath, galleryIDStr, img.Filename)
		res = append(res, r)
	}
	c.JSON(http.StatusOK, res)
}
