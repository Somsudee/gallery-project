package context

import (
	"gallery-api/models"

	"github.com/gin-gonic/gin"
)

func User(c *gin.Context) *models.User{
	user, ok := c.Value("user").(*models.User)
	if !ok {
		return nil
	}
	return user
}