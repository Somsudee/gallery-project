package main

import (
	"gallery-api/config"
	"gallery-api/handlers"
	"gallery-api/middleware"
	"gallery-api/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true) // dev only!
	}

	if err := db.AutoMigrate(
		&models.Gallery{},
		&models.User{},
		&models.Image{},
	).Error; err != nil {
		log.Fatal(err)
	}

	gg := models.NewGalleryGorm(db)
	gh := handlers.NewGalleryHandler(gg)

	//User
	ug := models.NewUserGorm(db, conf.HMACKey)
	uh := handlers.NewUserHandler(ug)

	//Image
	ig := models.NewImageService(db)
	ih := handlers.NewImageHandler(gg, ig)

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowHeaders = []string{"authorization", "content-type"}
	config.AllowOrigins = []string{"https://frontgallery2.twilightparadox.com"}
	r.Use(cors.New((config)))
	r.Static("/upload", "./upload")

	r.POST("/register", uh.CreateUser)
	r.POST("/login", uh.Login)
	r.GET("/galleries", gh.ListGallery)
	r.GET("/galleries/:id", gh.GetOne)
	r.GET("/galleries/:id/image", ih.ListGalleryImages)
	authorized := r.Group("/")
	authorized.Use(middleware.RequiredUser(ug))
	{
		//user
		authorized.POST("/logout", uh.Logout)
		authorized.GET("/session", func(c *gin.Context) {
			user, ok := c.Value("user").(*models.User)
			if !ok {
				c.JSON(401, gin.H{
					"message": "invalid token",
				})
				return
			}
			c.JSON(200, user)
		})
		authorized.GET("/user", uh.GetSession)
		//gallery
		authorized.POST("/galleries", gh.CreateGallery)
		authorized.DELETE("/galleries/:id", gh.DeleteGallery)
		authorized.PUT("/galleries/:id", gh.UpdateGallery)
		authorized.PUT("/galleries/:id/publishes", gh.UpdatePublishing)
		//image
		authorized.POST("/galleries/:id/upload", ih.CreateImage)
		authorized.DELETE("/image/:id", ih.DeleteImage)
	}
	r.Run()

}
