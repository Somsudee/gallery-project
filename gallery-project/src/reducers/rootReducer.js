import { combineReducers } from 'redux';
import { LoginReducer } from "./LoginReducer";
import { UserReducer } from './UserReducer';

const rootReducer = combineReducers({
  checklogin: LoginReducer,
  userReducer: UserReducer,
});
export default rootReducer;