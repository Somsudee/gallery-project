import { TOKEN } from "../actions/LoginAction"

export const initialLogin = {
  token: "",
}

export const LoginReducer = (state = initialLogin, action) => {
  switch (action.type) {
    case TOKEN:
      return {
        token: action.token,
      }
    default:
      return state
  }
}
