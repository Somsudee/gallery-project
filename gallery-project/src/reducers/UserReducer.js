import { SET_USER } from "../actions/UserAction"


export const initialUser = {
  user: {},
}

export const UserReducer = (state = initialUser, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        user: action.user,
      }
    default:
      return state
  }
}
