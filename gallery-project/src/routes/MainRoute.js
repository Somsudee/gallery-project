import React, { useEffect } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Login from '../components/login/Login';
import AddPhoto from '../components/admin/AddPhoto';
import Register from '../components/register/Register';
import UserHome from '../components/user/UserHome';
import AdminHome from '../components/admin/AdminHome';
import ViewPhoto from '../components/user/ViewPhoto';
import AdminGallery from '../components/admin/AdminGallery';
import Profile from '../components/admin/Profile';
import PrivateRoute from './PrivateRoute';
import { OnLogout } from '../api';

const MainRoute = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" render={() => {
        return <Redirect to="/home" />
      }} />
      <Route path="/processLogout" render={() => {
        OnLogout()
        return <Redirect to="/" />
      }}></Route>

      <Route exact path="/home" component={UserHome} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/viewGallery/:id_gallery" component={ViewPhoto} />
      <PrivateRoute exact path="/adminHome" component={AdminHome} />
      <PrivateRoute exact path="/addPhoto/:id_gallery" component={AddPhoto} />
      <PrivateRoute exact path="/adminGallery/:id_user" component={AdminGallery} />
      <PrivateRoute exact path="/profile" component={Profile} />
    </BrowserRouter>
  )
}
export default MainRoute;