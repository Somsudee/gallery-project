import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ path, component: Component, ...props }) => {
const {token} = useSelector(state => state.checklogin)
     
return(
        <Route path={path} render={(props) => {

            if (token != "") {
                return <Component {...props} />
            }
            else {
                return <Redirect to="/" />
            }
        }} {...props}/>
    )
}

export default PrivateRoute;
