import axios from 'axios';

export const host = "https://backgallery.twilightparadox.com";

// export const host = "http://localhost:8080";
//gallery
export function oneGallery(id) {
  return axios.get(`${host}/galleries/${id}`);
}

export function listGallery() {
  return axios.get(`${host}/galleries`);
}

export function createGallery(Name, UserID, Status, token) {
  return axios.post(`${host}/galleries`, {
    Name,
    UserID,
    Status,
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function deleteGallery(id, token) {
  return axios.delete(`${host}/galleries/${id}`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function updateStatus(id, Status, token) {
  return axios.put(`${host}/galleries/${id}/publishes`, {
    Status,
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function updateGallery(galleries, id, Status, token) {
  return axios.put(`${host}/galleries/${id}`, {
    Name: galleries.Name,
    UserID: galleries.UserID,
    Status,
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}


//image
export function listImage(id) {
  return axios.get(`${host}/galleries/${id}/image`);
}

export function createImage(galleryId, token) {
  return axios.post(`${host}/galleries/${galleryId}/image`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function deleteImage(id, token) {
  return axios.delete(`${host}/image/${id}`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function postImage(form, galleryId, token) {
  return axios.post(`${host}/galleries/${galleryId}/upload`, form,
    {
      headers: {
        Authorization: `Bearer ` + token,
      }
    })
}


//user 
export function CreateUser(newUser) {
  return axios.post(`${host}/register`, {
    Email: newUser.email,
    Password: newUser.password,
    FirstName: newUser.firstname,
    LastName: newUser.lastname,
    Gender: newUser.gender,
  })
}

export function OnLogin(user) {
  return axios.post(`${host}/login`, {
    Email: user.email,
    Password: user.password,
  })
}

export function OnLogout(token) {
  return axios.post(`${host}/logout`,
    {},
    {
      headers: {
        Authorization: `Bearer ` + token,
      }
    })
}

export function getUser(token) {
  return axios.get(`${host}/user`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

