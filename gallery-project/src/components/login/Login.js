import React, { useState } from 'react';
import { Row, Col, Typography, Input, Button } from 'antd';
import { useHistory } from 'react-router-dom';
import pearLogo from '../../asset/pear.png';
import { OnLogin, getUser } from '../../api/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getToken } from '../../actions/LoginAction';
import { PlusCircleFilled, } from '@ant-design/icons';

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { getToken } = props;
  const history = useHistory();

  const login = () => {
    var user = {
      email,
      password,
    }

    OnLogin(user).then((res) => {
      getToken(res.data.token);
      getUser(res.data.token);
      history.push("/adminHome");
    })
      .catch((err) => {
        alert(err)
      });
    setEmail("");
    setPassword("");
  }

  return (
    <Row
      justify="center"
      align="middle"
      style={{
        minHeight: "100vh",
        paddingTop: "32px",
      }}
    >
      <Col
        justify="center"
        align="middle"
        style={{
          height: 550,
          width: 500,
          borderRadius: 8,
          padding: 16,
          border: '1px solid #F1F1F1'
        }}
      >
        <Typography style={{ fontSize: 32 }}>Login</Typography>
        <img
          url='logo'
          src={pearLogo}
          style={{
            height: 150,
            width: 150,
            margin: 24,
          }}
        />
        <Row justify="center">
          <Input
            size="large"
            placeholder="Email"
            style={{
              borderRadius: 8,
              margin: 8,
              width: 400,
              marginBottom: 16,
            }}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Row>
        <Row justify="center">
          <Input.Password
            size="large"
            placeholder="Password"
            style={{
              borderRadius: 8,
              margin: 8,
              width: 400,
              marginBottom: 16,
            }}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Row>
        <Row justify="center">
          <Button
            size="large"
            type="danger"
            style={{
              width: 100,
              borderRadius: 8,
            }}
            disabled={email == "" || password == ""}
            onClick={() => login()}
          >Login</Button>
        </Row>
        <p style={{ margin: 8 }}>or</p>
        <Button
          size="large"
          style={{
            width: 100,
            borderRadius: 8
          }}
          onClick={() => history.push('/register')}
        >Register</Button>
      </Col>

    </Row>
  )
}

const mapStateToProps = (state) => {
  return {}
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({ getToken }, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(Login);