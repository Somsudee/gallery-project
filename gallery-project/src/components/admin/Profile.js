import React from 'react';
import { Row, Col, Divider } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from './Header';

const Profile = (props) => {

  const { user } = props

  return (
    <div>
      <Header />
      <Row
        justify="center"
        align="middle"
        style={{
          minHeight: "60vh",
          minWidth: "100%",
          fontFamily: "Open Sans Condensed, sans-serif",
        }}
      >
        <Col
          align="middle"
          style={{
            minWidth: "50%",
            fontFamily: "Open Sans Condensed, sans-serif",
          }}
        >
          <h1 style={{ fontSize: 48 }}>{user.FirstName.toUpperCase() + " " + user.LastName.toUpperCase()} </h1>
          <Divider orientation="center" plain >Profile</Divider>
          <Row justify="start">
            <Col>
              <b style={{ fontSize: 16, marginRight: 32 }}>Email: </b>
            </Col>
            <Col>
              <p style={{ fontSize: 16 }}>{user.Email}</p>
            </Col>
          </Row>
          <Row justify="start">
            <Col>
              <b style={{ fontSize: 16, marginRight: 24 }}>Gender: </b>
            </Col>
            <Col>
              <p style={{ fontSize: 16 }}>{user.Gender}</p>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.userReducer.user,
    token: state.checklogin.token
  }
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(Profile);