import React from 'react';
import { useHistory } from 'react-router-dom';
import { Menu, Typography, Row, Col, Dropdown } from 'antd';
import pearLogo from '../../asset/pear.png';
import { OnLogout } from '../../api';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getToken } from '../../actions/LoginAction';
import { setUser } from '../../actions/UserAction';
import { DownOutlined } from '@ant-design/icons';

const Header = (props) => {
  const { token, user, getToken, setUser } = props;
  const history = useHistory();

  const logout = () => {
    OnLogout(token).then(() => {
      getToken("");
      setUser("");
      history.push('/');
    })
  }

  const { SubMenu } = Menu;

  const menu = (
    <Menu
      style={{
        fontFamily: "Open Sans Condensed, sans-serif",
        width: 100,
      }}>
      <Menu.Item onClick={() => history.push('/profile')}>Profile</Menu.Item>
      <Menu.Divider />
      <Menu.Item onClick={() => logout()} style={{ color: "#F6769C" }} >Logout</Menu.Item>
    </Menu>
  );

  return (
    <Row
      align="middle"
      style={{
        backgroundColor: '#fff',
        borderBottom: '1px solid #F1F1F1',
        fontFamily: "Open Sans Condensed, sans-serif",
      }}
    >
      <Col
        span={20}>
        <Row align="middle" >
          <img
            url='logo'
            src={pearLogo}
            style={{
              height: 32,
              width: 32,
              margin: 8,
              marginRight: 4,
            }}
          />
          <Typography
            style={{
              fontFamily: "Open Sans Condensed, sans-serif",
              fontSize: 24,
              marginRight: 32,
            }}
          >PEAR</Typography>
          <Menu
            mode="horizontal"
            style={{ lineHeight: '64p', fontSize: 16, }}
          >
            <Menu.Item key="1"
              onClick={() => history.push('/adminHome')}
            >
              Home
            </Menu.Item>
            <Menu.Item key="2"
              onClick={() => history.push('/adminGallery/'+user.ID)}
            >
              Owner
            </Menu.Item>
          </Menu>
        </Row>
      </Col>
      <Col align="middle" span={4}>
        <Dropdown.Button
          overlay={menu}
          icon={<DownOutlined />}
          style={{ fontSize: 32, fontFamily: "Open Sans Condensed, sans-serif" }}
          onClick={() => history.push("/profile")}
        >
          {user.FirstName}
        </Dropdown.Button>
      </Col>
    </Row>
  )
}

const mapStateToProps = (state) => {
  return {
    token: state.checklogin.token,
    user: state.userReducer.user,
  }
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({ getToken, setUser }, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(Header);