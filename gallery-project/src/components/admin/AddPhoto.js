import React, { useState, useEffect } from 'react';
import { Upload, Col, Row, Divider, Typography, Button, Input, Modal, Dropdown, Menu, Popover } from 'antd';
import { UploadOutlined, DownOutlined, DeleteOutlined } from '@ant-design/icons';
import Header from './Header';
import { deleteGallery, oneGallery, listImage, updateStatus, postImage, updateGallery, deleteImage, host } from '../../api';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Carousel, { Modal as ModalImage, ModalGateway } from 'react-images';
import { useHistory } from 'react-router-dom';

const AddPhoto = (props) => {
  const [gallery, setGallery] = useState({});
  const [images, setImages] = useState([]);
  const [antImages, setAntImages] = useState([]);
  const [titleGallery, setTitleGallery] = useState("");
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [status, setStatus] = useState("");
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const history = useHistory();
  const { token, user } = props;

  useEffect(() => {
    fetchGallery()
    fetchImages()
  }, [])

  const fetchGallery = () => {
    oneGallery(props.location.state.id)
      .then((response) => setGallery(response.data))
      .catch((err) => console.error(err));
  }

  const fetchImages = () => {
    listImage(props.location.state.id)
      .then((res) => setImages(res.data))
      .catch((err) => console.log(err));
  }

  const changeStatus = (s) => {
    s = !s;
    onStatus(s);
    setStatus(s);
  }

  const onStatus = (status) => {
    updateStatus(gallery.ID, status, token)
      .then(() => fetchGallery())
      .catch((err) => console.log(err));
  }

  const uploadImage = () => {
    const form = new FormData();
    for (let i = 0; i < antImages.length; i++) {
      form.append("photos", antImages[i]);
    }
    postImage(form, gallery.ID, token)
      .then((res) => {
        setImages([...images, ...res.data]);
      })
      .catch((err) => {
        console.error(err);
      });
    console.log(images)
  }

  const handleOk = () => {
    setConfirmLoading(true)
    updateGallery(titleGallery, user.ID, status, token).then((res) => setGallery(res.data))
    setTimeout(() => {
      setVisible(false)
      setConfirmLoading(false)
    }, 1000);
  };

  const handleCancel = () => {
    setVisible(false)
  };

  const showModal = () => {
    setVisible(true)
  };

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen)
  };

  const onDelete = (id) => {
    if (images.length != 0) {
      alert("Required to clear images in gallery")
    }
    deleteGallery(id, token).then(() => fetchGallery()).catch((err) => console.log(err))
    history.push("/adminGallery/" + user.ID)
  }

  const onDeleteImage = (id) => {
    deleteImage(id, token).then(() => fetchImages()).catch((err) => console.log(err))
  }

  const menu = (
    <Menu>
      <Menu.Item key="0" onClick={() => showModal()}>
        Edit Title
    </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="2" onClick={() => onDelete(gallery.ID)}>
        Delete this gallery
    </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Header />
      <Row
        justify="center"
        style={{
          marginTop: 16,
          minHeight: "100vh",
          minWidth: "100%",
          fontFamily: "Open Sans Condensed, sans-serif",
        }}>
        <Col
          justify="center"
          align="middle"
          style={{
            minWidth: "80%",
          }}
        >
          <Row
            justify="center"
            style={{ margin: 8 }}
          >
            <Typography style={{ fontSize: 48, marginRight: 24 }}>{gallery.Name}</Typography>
            <Col>
              <Row>
                {
                  gallery.Status == false ?
                    <Button type="danger" size="small" onClick={() => changeStatus(gallery.Status)} style={{ width: 88 }}>
                      Private
                    </Button>
                    :
                    <Button type="primary" size="small" onClick={() => changeStatus(gallery.Status)} style={{ width: 88 }}>
                      Public
                    </Button>
                }
              </Row>
              <Row>
                <Dropdown overlay={menu}>
                  <Button size="small" style={{ width: 88 }}>
                    Edit Gallery <DownOutlined />
                  </Button>
                </Dropdown>
                <Modal
                  title="Add Gallery"
                  visible={visible}
                  onOk={() => handleOk()}
                  confirmLoading={confirmLoading}
                  onCancel={() => handleCancel()}
                >
                  <Input
                    size="large"
                    placeholder="Title"
                    value={titleGallery}
                    onChange={e => setTitleGallery(e.target.value)}
                  />
                </Modal>
              </Row>
            </Col>
          </Row>
          <Row
            align="middle"
            justify="center"
          >
            <Col>
              <Upload
                multiple
                onChange={({ file, fileList, event }) => {
                  console.log("file", file);
                  console.log("fileList", fileList);
                  console.log("event", event);
                  setAntImages(fileList.map((f) => f.originFileObj));
                }}
              >
                <Button
                  style={{ margin: 8 }}
                >
                  <UploadOutlined /> Click to Choose files
              </Button>
              </Upload>
              <Button
                onClick={() => uploadImage()}
                style={{ margin: 8 }}
              >
                Upload
            </Button>
            </Col>
          </Row>
          <Row>
            <Divider orientation="left" plain style={{ marginTop: 16, marginBottom: 16 }}>Photo</Divider>
          </Row>
          <Row>
            <Col >
              <ModalGateway>
                {
                  modalIsOpen ?
                    <ModalImage onClose={() => toggleModal()}>
                      <Carousel views={
                        images?.map((img) => {
                          return {
                            source: {
                              download: `${host}/${img.Filename}`,
                              fullscreen: `${host}/${img.Filename}`,
                              regular: `${host}/${img.Filename}`,
                              thumbnail: `${host}/${img.Filename}`
                            }
                          }
                        })
                      } />
                    </ModalImage>
                    :
                    null
                }
              </ModalGateway>
              {
                images.map((image, index) => {
                  console.log(image)
                  return (
                    <>
                      <Popover
                        content={
                          <Button
                            type="danger"
                            icon={<DeleteOutlined />}
                            onClick={() => onDeleteImage(image.ID)}
                          >Delete Image</Button>
                        }
                        title="Edit Image"
                      >
                        <img
                          key={index}
                          url='logo'
                          src={`${host}/${image.Filename}`}
                          style={{
                            margin: 4,
                            width: 400,
                            marginRight: 8
                          }}
                          onClick={() => setModalIsOpen(!modalIsOpen)}
                        />
                      </Popover>
                    </>
                  )
                })
              }
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  )
}
const mapStateToProps = (state) => {
  return {
    token: state.checklogin.token,
    user: state.userReducer.user,
  }
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(AddPhoto);