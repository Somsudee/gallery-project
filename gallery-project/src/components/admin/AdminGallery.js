import React, { useState, useEffect } from 'react';
import Header from './Header';
import { Row, Col, Button, Modal, Input, Card, Empty } from 'antd';
import { PlusCircleFilled } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { listGallery, createGallery } from '../../api';
import defaultImage from '../../asset/defaultImage.jpg';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const AdminGallery = (props) => {
  const [galleries, setGalleries] = useState([]);
  const [images, setImages] = useState([]);

  const [filter, setFilter] = useState([]);
  const [search, setSearch] = useState("");

  const [titleGallery, setTitleGallery] = useState("");
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [status, setStatus] = useState(false);

  const { user, token } = props
  const history = useHistory();
  const { Search } = Input;
  const { Meta } = Card;

  useEffect(() => {
    fetchGalleries()
  }, [])

  const fetchGalleries = () => {
    listGallery()
      .then((response) => setGalleries(response.data))
      .catch((err) => console.error(err));
  }

  const handleOk = () => {
    setConfirmLoading(true)
    createGallery(titleGallery, user.ID, status, token).then(() => fetchGalleries())
    setTimeout(() => {
      setVisible(false)
      setConfirmLoading(false)
    }, 2000);
  };

  const handleCancel = () => {
    setVisible(false)
  };

  const showModal = () => {
    setVisible(true)
  };

  const onSearch = (text) => {
    setSearch(text);
    galleries.filter((g) => {
      if (g.Name == text) {
        return filter.push(g)
      }
    })
  }

  return (
    <div>
      <Header />
      <Row
        justify="center"
        style={{
          minHeight: "100vh",
          minWidth: "100%",
          fontFamily: "Open Sans Condensed, sans-serif",
        }}
      >
        <Col
          justify="center"
          style={{
            minHeight: "100vh",
            minWidth: "80%",
            paddingTop: 16,
          }}
        >
          <Row justify="end" >
            <Button
              size="large"
              onClick={() => showModal()}
              style={{
                borderRadius: 8,
                margin: 16,
                justifyContent: 'center',
                backgroundColor: '#F6769C',
                color: '#fff',
              }}
              icon={
                <PlusCircleFilled
                  style={{
                    fontSize: 18,
                  }}
                />
              }
            >Gallery</Button>
            <Modal
              title="Add Gallery"
              visible={visible}
              onOk={() => handleOk()}
              confirmLoading={confirmLoading}
              onCancel={() => handleCancel()}
            >
              <Input
                size="large"
                placeholder="Title"
                onChange={e => setTitleGallery(e.target.value)}
              />
            </Modal>
          </Row>
          <Row justify="center" span={24}>
            <Search
              placeholder="Find your galleries"
              onSearch={e => onSearch(e)}
              style={{ width: 400, margin: 16 }}
            />
          </Row>
          <Row justify='center' align='middle'>
            {
              galleries.length == 0 ?
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  imageStyle={{
                    height: 60,
                    margin: 16,
                  }}
                  description={
                    <span style={{ fontSize: 32, fontFamily: "Open Sans Condensed, sans-serif", }}>
                      No Gallery
                    </span>
                  }
                >
                  <Button
                    type="primary"
                    size='large'
                    onClick={() => showModal()}
                    style={{
                      borderRadius: 8,
                      margin: 16,
                      justifyContent: 'center',
                      backgroundColor: '#F6769C',
                      color: '#fff',
                      borderColor: '#F6769C',
                    }}
                  >Create Now</Button>
                </Empty>
                :
                search == "" ?
                  galleries.map((g, index) => {
                    return (
                      <>
                        {
                          g.UserID != user.ID ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/addPhoto/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })
                  :
                  filter.map((g, index) => {
                    return (
                      <>
                        {
                          g.Status == false ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/addPhoto/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })

            }
          </Row>
        </Col>
      </Row>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.userReducer.user,
    token: state.checklogin.token
  }
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(AdminGallery);