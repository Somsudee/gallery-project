import React, { useState, useEffect } from 'react';
import { Col, Row, Input, Button, Card, Empty, Modal } from 'antd';
import { PlusCircleFilled, } from '@ant-design/icons';
import { useHistory } from 'react-router';
import Header from './Header';
import pearLogo from '../../asset/pear.png';
import defaultImage from '../../asset/defaultImage.jpg';
import { createGallery, listGallery, getUser, listImage } from '../../api/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser } from '../../actions/UserAction';

const AdminHome = (props) => {
  const [galleries, setGalleries] = useState([]);
  const [images, setImages] = useState([]);
  const [filter, setFilter] = useState([]);
  const [search, setSearch] = useState("");

  const [titleGallery, setTitleGallery] = useState("");
  const [status, setStatus] = useState(false);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const { token, setUser, user } = props;
  const history = useHistory();
  const { Search } = Input;
  const { Meta } = Card;

  useEffect(() => {
    fetchGalleries()
    fetchUser(token)
  }, [])

  const fetchGalleries = () => {
    listGallery()
      .then((response) => setGalleries(response.data))
      .catch((err) => console.error(err));
  }

  const fetchUser = (token) => {
    getUser(token)
      .then((res) => setUser(res.data))
      .catch((err) => console.error(err));
  }

  const handleOk = () => {
    setConfirmLoading(true)
    createGallery(titleGallery, user.ID, status, token).then(() => fetchGalleries())
    setTimeout(() => {
      setVisible(false)
      setConfirmLoading(false)
    }, 1000);
  };

  const handleCancel = () => {
    setVisible(false)
  };

  const showModal = () => {
    setVisible(true)
  };

  const onSearch = (text) => {
    setSearch(text);
    galleries.filter((g) => {
      if (g.Name == text) {
        return filter.push(g)
      }
    })
  }

  return (
    <>
      <Header />
      <Row
        justify="center"
        style={{
          minHeight: "100vh",
          minWidth: "100%",
        }}
      >
        <Col
          justify="center"
          // align="middle"
          style={{
            minHeight: "100vh",
            minWidth: "80%",
            paddingTop: 16,
          }}
        >
          <Row justify="end" >
            <Button
              size="large"
              onClick={() => showModal()}
              style={{
                borderRadius: 8,
                margin: 16,
                justifyContent: 'center',
                backgroundColor: '#F6769C',
                color: '#fff',
                fontFamily: "Open Sans Condensed, sans-serif",
              }}
              icon={
                <PlusCircleFilled
                  style={{
                    fontSize: 18,
                  }}
                />
              }
            >Gallery</Button>
            <Modal
              title="Add Gallery"
              visible={visible}
              onOk={() => handleOk()}
              confirmLoading={confirmLoading}
              onCancel={() => handleCancel()}
            >
              <Input
                size="large"
                placeholder="Title"
                onChange={e => setTitleGallery(e.target.value)}
              />
            </Modal>
          </Row>
          <Row justify="center" align="middle">
            <img
              url='logo'
              src={pearLogo}
              style={{
                height: 200,
                width: 200,
                margin: 8,
              }}
            />
          </Row>
          <Row justify="center" span={24}>
            <Search
              placeholder="Find galleries"
              // onSearch={value => onSearch(value)}
              onChange={e => onSearch(e.target.value)}
              style={{ width: 400, margin: 16, fontFamily: "Open Sans Condensed, sans-serif", }}
            />
          </Row>
          <Row justify='center' align='middle'>
            {
              galleries.length == 0 ?
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  imageStyle={{
                    height: 60,
                    margin: 16,
                  }}
                  description={
                    <span style={{ fontSize: 32, fontFamily: "Open Sans Condensed, sans-serif", }}>
                      No Gallery
                    </span>
                  }
                >
                  <Button
                    type="primary"
                    size='large'
                    onClick={() => showModal()}
                    style={{
                      borderRadius: 8,
                      margin: 16,
                      justifyContent: 'center',
                      backgroundColor: '#F6769C',
                      color: '#fff',
                      borderColor: '#F6769C',
                    }}
                  >Create Now</Button>
                </Empty>
                :
                search == "" ?
                  galleries.map((g, index) => {
                    return (
                      <>
                        {
                          g.Status == false ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/addPhoto/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })
                  :
                  filter.map((g, index) => {
                    return (
                      <>
                        {
                          g.Status == false ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/addPhoto/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })

            }
          </Row>
        </Col>
      </Row>
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    token: state.checklogin.token,
    user: state.userReducer.user,
  }
}

const mapDisPatchToProps = (dispatch) => {
  return bindActionCreators({ setUser }, dispatch)
}
export default connect(mapStateToProps, mapDisPatchToProps)(AdminHome);