import React, { useState } from 'react';
import { Row, Col, Typography, Input, Radio, Button } from 'antd';
import { CreateUser } from '../../api/index';
import { useHistory } from 'react-router-dom';

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [gender, setGender] = useState("")

  const history = useHistory();

  const radioStyle = {
    height: 30,
    margin: 16,
    marginBottom: 0,
    fontWeight: 400,
    fontSize: 20,
    fontFamily: "Open Sans Condensed, sans-serif",
  };

  const onChange = e => {
    console.log('radio checked', e.target.value);
    setGender(e.target.value)
  };

  const onCreateAccount = () => {
    var newUser = {
      email,
      password,
      firstname,
      lastname,
      gender,
    }
    CreateUser(newUser).then(() => history.push("/login"))
  }

  return (
    <Row
      justify="center"
      style={{
        minHeight: "100vh",
        padding: 150,
      }}
    >
      <Col
        justify="center"
        align="middle"
        style={{
          height: 400,
          width: 600,
          borderRadius: 8,
        }}
      >
        <Row
          justify="center"
        >
          <Typography
            style={{
              fontSize: 60,
              margin: 16,
              fontWeight: 500,
              fontFamily: "Open Sans Condensed, sans-serif",
            }}>Register</Typography>
        </Row>
        <Row>
          <Input
            size="large"
            placeholder="Email"
            style={{
              margin: 16,
              borderRadius: 8,
            }}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Row>
        <Row>
          <Input.Password
            size="large"
            placeholder="Password"
            style={{
              margin: 16,
              borderRadius: 8,
            }}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Row>
        <Row>
          <Input.Password
            size="large"
            placeholder="Confirmed Password"
            style={{
              margin: 16,
              borderRadius: 8,
            }}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
          {
            password != confirmPassword ?
              <p style={{ color: "red", marginLeft: 16 }}>Password is not same with comfirmed password</p>
              :
              null
          }
        </Row>
        <Row style={{ margin: 16, }}>
          <Input
            size="large"
            placeholder="Firstname"
            style={{
              marginRight: 8,
              borderRadius: 8,
              width: 275,
            }}
            onChange={(e) => setFirstname(e.target.value)}
          />
          <Input
            size="large"
            placeholder="Lastname"
            style={{
              marginLeft: 8,
              borderRadius: 8,
              width: 275,
            }}
            onChange={(e) => setLastname(e.target.value)}
          />
        </Row>
        <Row justify="center" align="middle" style={{ margin: 16, }}>
          <Radio.Group onChange={(e) => onChange(e)} value={gender}>
            <Radio size="large" style={radioStyle} value={"female"}>
              Female
            </Radio>
            <Radio size="large" style={radioStyle} value={"male"}>
              Male
            </Radio>
          </Radio.Group>
        </Row>
        <Row justify="center" >
          <Button
            size='large'
            style={{
              borderRadius: 8,
              width: 200,
              margin: 16,
            }}
            type="danger"
            disabled={email == "" || password == "" || firstname == "" || lastname == "" || gender == ""}
            onClick={() => onCreateAccount()}
          >Create Account</Button>
        </Row>

      </Col>

    </Row>
  )
}
export default Register;