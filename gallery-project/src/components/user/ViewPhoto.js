import React, { useState, useEffect } from 'react';
import UserHeader from './UserHeader';
import { Row, Col, Typography, Divider } from 'antd';
import { oneGallery, listImage, host } from '../../api';
import Carousel, { Modal as ModalImage, ModalGateway } from 'react-images';

const ViewPhoto = (props) => {
  const [gallery, setGallery] = useState({});
  const [images, setImages] = useState([]);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    fetchGallery()
    fetchImages()
  }, [])

  const fetchGallery = () => {
    oneGallery(props.location.state.id)
      .then((response) => setGallery(response.data))
      .catch((err) => console.error(err));
  }

  const fetchImages = () => {
    listImage(props.location.state.id)
      .then((res) => setImages(res.data))
      .catch((err) => console.log(err));
  }

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen)
  };

  return (
    <div>
      <UserHeader />
      <Row
        justify="center"
        style={{
          minHeight: "100vh",
          minWidth: "100%",
        }}
      >
        <Col
          justify="center"
          align="middle"
          style={{
            minWidth: "80%",
          }}>
          <Typography style={{ fontSize: 48, margin: 24 }}>{gallery.Name}</Typography>
          <Divider orientation="left" plain >Photo</Divider>
          <Row
            justify="center"
            align="middle"
          >
            <Col>
              <ModalGateway>
                {
                  modalIsOpen ?
                    <ModalImage onClose={() => toggleModal()}>
                      <Carousel views={
                        images.map((img) => {
                          return {
                            source: {
                              download: `${host}/${img.Filename}`,
                              fullscreen: `${host}/${img.Filename}`,
                              regular: `${host}/${img.Filename}`,
                              thumbnail: `${host}/${img.Filename}`
                            }
                          }
                        })
                      } />
                    </ModalImage>
                    :
                    null
                }
              </ModalGateway>
              {
                images.map((image, index) => {
                  console.log(image.Filename)
                  return (
                    <img
                      key={index}
                      url='logo'
                      src={`${host}/${image.Filename}`}
                      style={{
                        margin: 4,
                        width: 400,
                      }}
                      onClick={() => setModalIsOpen(!modalIsOpen)}
                    />
                  )
                })
              }
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}
export default ViewPhoto;