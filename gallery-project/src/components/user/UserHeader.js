import React from 'react';
import { useHistory } from 'react-router-dom';
import { Menu, Typography, Row, Col, Button } from 'antd';
import pearLogo from '../../asset/pear.png';

const UserHeader = () => {

  const history = useHistory();

  return (
    <Row
      // justify="center"
      align="middle"
      style={{
        backgroundColor: '#fff',
        borderBottom: '1px solid #F1F1F1',
      }}
    >
      <Col
        span={22}>
        <Row align="middle" >
          <img
            url='logo'
            src={pearLogo}
            style={{
              height: 32,
              width: 32,
              margin: 8,
              marginRight: 4,
            }}
          />
          <Typography
            style={{
              fontFamily: "Open Sans Condensed, sans-serif",
              fontSize: 24,
              marginRight: 32,
            }}
          >PEAR</Typography>
          <Menu
            mode="horizontal"
            style={{ lineHeight: '64p', fontSize: 16, fontFamily: "Open Sans Condensed, sans-serif", }}
          >
            <Menu.Item key="1" onClick={() => {
              history.push('/');
            }}>
              Home
        </Menu.Item>
          </Menu>
        </Row>
      </Col>
      <Col span={2}>
        <Button style={{ borderRadius: 8, fontFamily: "Open Sans Condensed, sans-serif", }} onClick={() => history.push('/login')}>Login</Button>
      </Col>
    </Row>
  )
}
export default UserHeader;