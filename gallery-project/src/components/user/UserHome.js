import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Card, Empty } from 'antd';
import { useHistory } from 'react-router-dom';
import UserHeader from './UserHeader';
import pearLogo from '../../asset/pear.png';
import defaultImage from '../../asset/defaultImage.jpg';
import { listGallery, listImage } from '../../api';

const UserHome = () => {
  const [galleries, setGalleries] = useState([]);
  const [images, setImages] = useState([]);
  const [filter, setFilter] = useState([]);
  const [search, setSearch] = useState("");

  const history = useHistory();
  const { Search } = Input;
  const { Meta } = Card;

  useEffect(() => {
    fetchGalleries()
  }, [])

  const fetchGalleries = () => {
    listGallery()
      .then((response) => {
        setGalleries(response.data)
        fetchImages()
      })
      .catch((err) => console.error(err));
  }

  const fetchImages = () => {
    for (var i = 0; i < galleries.length; i++) {
      listImage(galleries[i].ID)
        .then((res) => setImages(res.data))
        .catch((err) => console.log(err));
    }
  }

  const onSearch = (text) => {
    setSearch(text);
    galleries.filter((g) => {
      if (g.Name == text) {
        return filter.push(g)
      }
    })
  }

  return (
    <div>
      <UserHeader />
      <Row
        justify="center"
        style={{
          minHeight: "100vh",
          paddingTop: 64,
        }}
      >
        <Col>
          <Row justify="center" align="middle">
            <img
              url='logo'
              src={pearLogo}
              style={{
                height: 150,
                width: 150,
                margin: 8,
              }}
            />
          </Row>
          <Row justify="center" span={24}>
            <Search
              placeholder="Find galleries"
              onChange={e => onSearch(e.target.value)}
              style={{ width: 400, margin: 16, fontFamily: "Open Sans Condensed, sans-serif", }}
            />
          </Row>
          <Row justify='center' align='middle'>
            {
              galleries.length == 0 ?
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  imageStyle={{
                    height: 60,
                    margin: 16,
                  }}
                  description={
                    <span style={{ fontSize: 24, fontFamily: "Open Sans Condensed, sans-serif", }}>
                      No Gallery
                    </span>
                  }
                />
                :
                search == "" ?
                  galleries.map((g, index) => {
                    return (
                      <>
                        {
                          g.Status == false ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/viewGallery/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })
                  :
                  filter.map((g, index) => {
                    return (
                      <>
                        {
                          g.Status == false ?
                            <></>
                            :
                            <Card
                              style={{
                                width: 400,
                                margin: 8,
                                borderRadius: 8,
                                fontFamily: "Open Sans Condensed, sans-serif",
                              }}
                              key={index}
                              hoverable
                              onClick={() => history.push("/viewGallery/" + g.ID, { id: g.ID })}
                              cover={
                                <img
                                  alt="example"
                                  style={{ height: 300 }}
                                  src={
                                    images.ID == 1 && images.length != 0 ?
                                      images.fileName
                                      :
                                      defaultImage
                                  } />
                              }
                            >
                              <Meta title={g.Name.toUpperCase()} />
                            </Card>
                        }
                      </>
                    )
                  })

            }
          </Row>
        </Col>
      </Row>
    </div>
  )
}
export default UserHome;